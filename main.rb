require './app/application'
require './app/option_parser'
require './app/clients/etsy'
require './app/clients/currency_layer'
require './app/utils/currency_converter'

SUPPORTED_CURRENCIES = ['USD','GBP','CAD','EUR']

# Parse and save CLI arguments
options = Wiivv::OptionParser.parse!

# Read env variables file depending on environment
environment_file_path = File.join('.', ".env.#{options[:environment]}.rb")
require environment_file_path if File.exists?(environment_file_path)

# TODO: improve by moving that into a cron job and giving the app access
# access to the latest exchange rates
rates = Wiivv::Clients::CurrencyLayer.new.get_rates(SUPPORTED_CURRENCIES)

# Start app with dependencies
Wiivv::Application.new(
    Wiivv::Clients::Etsy.new,
    Wiivv::Utils::CurrencyConverter.new(rates),
    options
).start