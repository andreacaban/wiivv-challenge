require 'net/http'

module Wiivv
    module Clients
        class CurrencyLayer
            BASE_URL = 'http://apilayer.net/api'.freeze

            def initialize
                raise EnvVarMissingError.new('CURRENCY_LAYER_API_KEY env var is missing') if ENV['CURRENCY_LAYER_API_KEY'].to_s.empty?
            end

            def get_rates(currencies)
                base_url = BASE_URL + '/live' + "?currencies=#{currencies.join(',')}&access_key=#{ENV['CURRENCY_LAYER_API_KEY']}"

                currencies.map do |currency|
                    # Note: with a free account, you cannot set the source other than 'USD'
                    # TODO: remove the following if account is upgraded
                    next unless currency == 'USD'

                    url = URI(base_url + "&source=#{currency}")
                    JSON.parse(Net::HTTP.get(url))['quotes']
                end.compact.reduce(&:merge)
            end
        end
    end
end