require 'net/http'

module Wiivv
    module Clients
        class Etsy
            BASE_URL = 'https://openapi.etsy.com/v2'.freeze

            def initialize
                raise EnvVarMissingError.new('ETSY_API_KEY env var is missing') if ENV['ETSY_API_KEY'].to_s.empty?
            end

            def get_top_listings(page=nil, limit=10, offset=0)
                url = BASE_URL + "/listings/active?limit=#{limit}&offset=#{offset}&sort_on=score&api_key=#{ENV['ETSY_API_KEY']}"
                url += "&page=#{page}" unless page.to_s.empty?
                listings = JSON.parse(Net::HTTP.get(URI(url)))

                [listings['results'], listings['pagination']['next_page']]
            end

            def get_listing_thumbnails(listing_id)
                url = URI(BASE_URL + "/listings/#{listing_id}/images?api_key=#{ENV['ETSY_API_KEY']}")

                JSON.parse(Net::HTTP.get(url))['results']
            end
        end
    end
end