require 'webrick'
require 'json'

module Wiivv
    class RESTServlet < WEBrick::HTTPServlet::AbstractServlet
        attr_reader :product_listing_client, :currency_converter

        def initialize(server, product_listing_client, currency_converter)
            super(server)
            @product_listing_client = product_listing_client
            @currency_converter = currency_converter
        end

        def do_GET (request, response)
            response.content_type = 'application/json'
            body = nil
            status = nil

            case request.path
            when '/rest/ping'
                status = 200
                body = 'pong!'
            when '/rest/v1/products'
                status = 200
                page = request.query['page']
                currency = request.query['currency'] || 'USD'

                listings, next_page = product_listing_client.get_top_listings(page)

                products = listings.map do |listing|
                    thumbnails = []
                    product_listing_client.get_listing_thumbnails(listing['listing_id']).each do |urls|
                        thumbnails << urls['url_fullxfull']
                    end
                    price = currency_converter.convert_from_to(
                        listing['price'],
                        listing['currency_code'],
                        currency
                    )
                    {}.tap do |product|
                        product[:title] = listing['title']
                        product[:id] = listing['listing_id']
                        product[:thumbnails] = thumbnails
                        product[:price] = price
                        product[:currency] = currency
                    end
                end

                body = JSON.generate({
                    products: products,
                    next_page: next_page
                })
            else
                status = 400
                body = 'No such rest resource'
            end

            response.status = status
            response.body = body + "\n"
        rescue CurrencyRateNotSupportedError => e
            response.status = 400
            response.body = e.message + "\n"
        end
    end
end