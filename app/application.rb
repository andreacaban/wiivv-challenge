require 'webrick'
require './app/rest_servlet'

module Wiivv
    class EnvVarMissingError < StandardError; end
    class CurrencyRateNotSupportedError < StandardError; end

    class Application
        attr_reader :port, :product_listing_client, :currency_converter

        def initialize(product_listing_client, currency_converter, options)
            @product_listing_client = product_listing_client
            @currency_converter = currency_converter
            @port = options[:port] || 1234
        end

        def start
            server = WEBrick::HTTPServer.new(:Port => port)
            server.mount('/rest', RESTServlet, product_listing_client, currency_converter)
            trap('INT') { server.shutdown }
            server.start
        end
    end
end