module Wiivv
    module Utils
        class CurrencyConverter
            attr_reader :rates

            def initialize(rates = {})
                @rates = {}
                add_rates(rates)
            end

            def convert_from_to(price = nil, source = nil, target = 'USD')
                rate = rates[source.to_s + target.to_s]

                raise CurrencyRateNotSupportedError.new("rate #{source + target} not supported at the moment") if rate.nil?

                (price.to_f * rate.to_f).round(2)
            end

            def add_rates(rates)
                rates.each { |rate, value| add_rate(rate, value) }
            end

            def add_rate(rate, value)
                @rates[rate] = value
                @rates[reverse_rate(rate)] = (1 / value.to_f).round(6)
            end

            private

            def reverse_rate(rate)
                [rate[0..2], rate[3..5]].reverse.join
            end
        end
    end
end