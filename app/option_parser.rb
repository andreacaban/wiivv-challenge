require 'optparse'

module Wiivv
    class OptionParser
        class PortArgumentMissing < ::OptionParser::MissingArgument; end
        class EnvironmentArgumentMissing < ::OptionParser::MissingArgument; end

        def self.parse!(options = {})
            parser = ::OptionParser.new do |opts|
                opts.banner = 'Usage: ruby main.rb [options]'

                opts.on(
                    '-eENVIRONMENT',
                    '--environment=ENVIRONMENT',
                    [:development, :production],
                    'Select environment to run app in (development, production)') do |v|
                    options[:environment] = v
                end

                opts.on('-pPORT', '--port=PORT', Integer, 'Set server port') do |v|
                    options[:port] = v
                end

                opts.on("-h", "--help", "Prints the help") do
                    puts opts
                    exit
                end
            end

            parser.parse!(options)
            raise PortArgumentMissing if options[:port].nil?
            raise EnvironmentArgumentMissing if options[:environment].nil?

            options
      end
    end
end