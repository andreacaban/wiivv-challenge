# Goal
Etsy is an online eCommerce store for handmade craft products. We want to build a simple API which gets top products from Etsy API and provides the list via an API in a list. The list should have the title of the item (any image thumbnail URL which is provided by the API). The API should be paginated and return 10 products on a single page of the product listing.

We also want to provide functionality to the user to get the list of products and their pricing in any currency they want. We want to support the following currencies:

1. GBP
2. CAD
3. USD
4. EUR

You can get the latest exchange rates from the public API https://currencylayer.com/. Currency rates only change hourly.

Base your solution on how you would write production ready code. The solution should show best of your coding style, project organization and other best practices.

Mail your solution to eric@wiivv.com

If you have read it this far, you can get a 25% discount on any of our products. Head over to https://wiivv.com/ and apply the discount code WIIVVCHALLENGE on the checkout page. :)

## Dependencies
1. install rvm
2. install gem
3. install bundler
```
$ gem install bundler
```

## Development
in the root folder of the app:

1) Make sure you are running ruby 2.4.1 with:
```
$ rvm list # => 2.4.1
```
2) If not, select it (or install it)
```
$ rvm use 2.4.1
($ rvm install 2.4.1) 
```
3) Install project dependencies
```
$ bundle install
```
4) copy the environment file and add your APIs credentials
```
cp ./.env.example.rb ./.env.development.rb
```
5) Run app
```
$ ruby main.rb -e development -p 1234 # => App is running in development environment at localhost:1234
```

## Troubleshoot
1) Problems and don't know where to start looking at, check the dependencies are met with:

`$ bundle check`

if outputs is smg like "The Gemfile's dependencies are satisfied", you are good on that end.

2) Api not working? Make sure the app is running, if so make sure it can serve request by curling the following endpoint:

`$ curl -v http://localhost:1234/rest/ping # => pong! `

3) App will not work if the environment variables are not set and will throw an error.

## REST API
### GET /rest/v1/products
#### Request
##### Query params
```
currency: [USD|GBP|CAD|EUR] (default is USD)
page: [Integer]
```

##### Example
```
curl -v http://localhost::1234/rest/v1/products?currency=CAD&page=3
```

#### Response
##### Example
```
OK 200
{
    "products": [
        {
          "title": "Vintage 1993 Alaska Outdoor All Over Print T-shirt Medium",
          "id": 620692007,
          "thumbnails": [
            "https://i.etsystatic.com/12996840/r/il/78c27c/1576138441/il_fullxfull.1576138441_mpdc.jpg",
            "https://i.etsystatic.com/12996840/r/il/e64c7b/1528672802/il_fullxfull.1528672802_3m7z.jpg",
            "https://i.etsystatic.com/12996840/r/il/a1182c/1576140009/il_fullxfull.1576140009_kwsf.jpg"
          ],
          "price": 39.06,
          "currency": "CAD"
        },
        {
          "title": "Navy blue hair bow - hair bow, hair bows, bows, hair bows for girls, baby bows, baby hair bows, toddler hair bows, girls bows, hairbows",
          "id": 190017302,
          "thumbnails": [
            "https://img.etsystatic.com/il/d69cbb/1287063128/il_fullxfull.1287063128_shy9.jpg?version=1",
            "https://img.etsystatic.com/il/605ea0/603883368/il_fullxfull.603883368_ck0q.jpg?version=0"
          ],
          "price": 5.21,
          "currency": "CAD"
        }
    ],
    "next_page": 4
}
```

## Further improvements
- only USD to or from rate conversions are supported as a free account has not access to other rates other than usd-xxx.
More [here](https://currencylayer.com/documentation) under "Source Currency Switching"
- Rates are updated only at startup, could be put in a cron job to update them every day.
- no specs were written as I didn't have enough time.
- No logger or logging file were added. It just prints to stdout.
- The servlet code is long. Should probably be refactored.
- The app can surely be broken as I did not have time to QA it properly either.